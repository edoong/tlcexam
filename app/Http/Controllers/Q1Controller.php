<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Q1Controller extends Controller
{

    //

    public function q1()
    {

        $iteration = 0;
        $prevlongChain = 0;
        $savedIterationNumber = 0;

        while ($iteration < 1000) {
            $iteration++;
            $end = $iteration;
            $chainIterator = 0;
            while ($end != 1) {
                $chainIterator++;
                // echo $end . "</br>";
                $end = $this->q1func($end);
            }
            // echo $end;
            echo " </br>Iteration: " . $iteration . " Longest chain: " . ($chainIterator + 1) . "</br>";

            if ($prevlongChain < ($chainIterator + 1)) {
                $savedIterationNumber = $iteration;
                $prevlongChain = ($chainIterator + 1);
            }
        }

        echo "</br><h1>Iteration: " . $savedIterationNumber . " produces the longest chain. Longest chain is: " . $prevlongChain . "</h1>";
    }

    public function q1func($n)
    {
        if ($n % 2 == 0) {
            $n = $n / 2;
        } else {
            $n = (3 * $n) + 1;
        }
        return $n;
    }



    public function q2()
    {
        $this->q2func(4000000);
    }

    public function q2func(int $upperLimit)
    {

        $curr = 1;
        $prev = 0;
        $evenarrays = array();

        while ($curr < $upperLimit) {
            echo $curr;
            $curr = $curr + $prev;
            $prev = ($curr - $prev);

            if ($curr % 2 == 0) {
                array_push($evenarrays, $curr);
            }
            echo "</br>";
        }
        echo "</br></br></br>";
        echo "even numbers are: </br>";
        $sumOfEvenNumbers = 0;
        foreach ($evenarrays as $evenNumbers) {
            echo "<h3>" . $evenNumbers . "</h3>";
            $sumOfEvenNumbers += $evenNumbers;
        }

        echo "<h1>The sum of even numbers is: " . $sumOfEvenNumbers . "</h1>";
    }

    public function q3()
    {

        echo $this->dateFashion(5, 10) . "</br>";
        echo $this->dateFashion(5, 2) . "</br>";
        echo $this->dateFashion(5, 5) . "</br>";

        //ensure integer data type
        // echo gettype($this->dateFashion(5, 10));
    }

    public function dateFashion(int $you, int $date): int
    {
        if (($you > 10 || $date > 10) || ($you < 0 || $date < 0)) {
            echo "range must is only between 0 to 10";
        } else {
            if ($you >= 8 || $date >= 8) {
                return 2;
            } else if ($you <= 2 || $date <= 2) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    public function q4()
    {
        $this->q4func(2, 5, 10);
    }

    public function q4func(int $a, int $b, int $c)
    {
        $intarrays = [$a, $b, $c];
        $sum = 0;

        foreach ($intarrays as $sourcekey => $source) {
            $returnsum = 0;
            $flagFound = false;
            foreach ($intarrays as $key => $data) {
                if ($sourcekey != $key && $data == $source) {
                    $flagFound = true;
                    $returnsum = 0;
                }

                if (!$flagFound) {
                    $returnsum = $source;
                }
            }
            $sum += $returnsum;
        }
        echo "The Sum is: " . $sum;
    }

    public function q5()
    {
        $this->luckySum(1, 2, 3);
        $this->luckySum(1, 2, 13);
        $this->luckySum(1, 13, 3);
    }
    public function luckySum(int $a, int $b, int $c)
    {
        $intarrays = [$a, $b, $c];
        $sum = 0;

        foreach ($intarrays as $source) {
            if ($source == 13) {
                break;
            } else {
                $returnsum = $source;
            }
            $sum += $returnsum;
        }
        echo "The Sum is: " . $sum . "</br>";;
    }

    public function q6()
    {
        $this->q6func();
    }

    public function q6func()
    {

        echo "The square of the sum of iteration 100 is " . number_format($this->squareOfTheSum(100)) . "</br>";
        echo "The sum of the square of iteration 100 is " . number_format($this->sumOfTheSquares(100));

        echo "<h1> The difference is " . number_format(($this->squareOfTheSum(100) - $this->sumOfTheSquares(100))) . "</br>";
    }

    public function squareOfTheSum(int $iteration)
    {
        $returnvalue = 0;
        for ($x = 0; $x <= $iteration; $x++) {
            $returnvalue = $x + $returnvalue;
        }

        return $returnvalue *= $returnvalue;
    }

    public function sumOfTheSquares(int $iteration)
    {
        $returnvalue = 0;
        for ($x = 0; $x <= $iteration; $x++) {
            $returnvalue +=   ($x * $x);
        }

        return $returnvalue;
    }
}
