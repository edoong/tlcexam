<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Q1Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('q1', [Q1Controller::class, 'q1']);
Route::get('q2', [Q1Controller::class, 'q2']);
Route::get('q3', [Q1Controller::class, 'q3']);
Route::get('q4', [Q1Controller::class, 'q4']);
Route::get('q5', [Q1Controller::class, 'q5']);
Route::get('q6', [Q1Controller::class, 'q6']);
